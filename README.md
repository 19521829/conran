# ConRan

#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <conio.h>
#include <string>
#include <ctime>
#define Trai 3
#define Phai 113
#define Tren 3
#define Duoi 23
using namespace std;
void gotoxy( int column, int line );
struct Point{
    int x,y;
};
class CONRAN{
public:
    struct Point A[100];
    int DoDai;
    CONRAN(){
        DoDai = 3;
        A[0].x = 10; A[0].y = 10;
        A[1].x = 11; A[1].y = 10;
        A[2].x = 12; A[2].y = 10;
    }
    void Ve(Point T){
        for (int i = 0; i < DoDai; i++)
        {
            gotoxy(A[i].x,A[i].y);
            cout<<"�";
        }
        gotoxy(T.x,T.y);
        cout<<" ";
    }
    Point DiChuyen(int Huong){
        Point ToaDoCuoiCu = A[DoDai-1];
        for (int i = DoDai-1; i>0;i--)
            A[i] = A[i-1];
        if (Huong==0) A[0].x = A[0].x + 1;
        if (Huong==1) A[0].y = A[0].y + 1;
        if (Huong==2) A[0].x = A[0].x - 1;
        if (Huong==3) A[0].y = A[0].y - 1;
        return ToaDoCuoiCu;
    }
    void vetuong ( )
    {
        for ( int i= Trai; i<= Phai; i++)
        {
            gotoxy(i,Tren);
            cout<<"�";
        }
        for ( int i= Tren; i<= Duoi; i++)
        {
            gotoxy(Trai,i);
            cout<<"�";
        }
        for ( int i= Trai; i<= Phai; i++)
        {
            gotoxy(i,Duoi);
            cout<<"�";
        }
        for ( int i= Tren; i<= Duoi; i++)
        {
            gotoxy(Phai,i);
            cout<<"�";
        }
    }

    bool kiemtraThua()
    {
        if ( A[0].y == Tren)
            return true;
        if ( A[0].y == Duoi)
            return true;
        if ( A[0].x == Trai)
            return true;
        if ( A[0].x == Phai)
            return true;
        return false;
    }
    void xuliThua()
    {
        if (kiemtraThua())
        {
            Sleep(500);
            system("cls");
            cout<<"\t\t\t\t\t\tGAME OVER!!!!!"<<endl;
        }
    }
    Point Moi()
    {
        srand(time(NULL));
        int x= Trai + 1 + rand() % (Phai - Trai -1);
        int y= Tren + 1 + rand() % (Duoi - Tren -1);
        gotoxy(x, y);
        cout<<"*";
        return Point{x,y};
    }
    bool kiemtraMoi(Point moi)
    {
        if (A[0].x==moi.x && A[0].y==moi.y)
            return true;
        return false;
    }
    void them()
    {
        A[DoDai]=A[DoDai-1];
        DoDai++;
    }
};


int main()
{
    CONRAN r;
    int Huong = 0, diem=0;
    char t;
    cout<<"\t\t\t\t\t\t\tGAME SNAKE"<<endl;
    r.vetuong();
    Point moi=r.Moi();
    while (1){
        if (kbhit())
        {
            t = getch();
            if (t=='a') Huong = 2;
            if (t=='w') Huong = 3;
            if (t=='d') Huong = 0;
            if (t=='s') Huong = 1;
        }
        Point ToaDoCuoiCu=r.DiChuyen(Huong);
        r.Ve(ToaDoCuoiCu);
        if (r.kiemtraThua()) break;
        if (r.kiemtraMoi(moi))
        {
            moi=r.Moi();
            r.them();
            diem++;
            gotoxy(Trai, Duoi +3);
            cout<<"DIEM: "<<diem;

        }
        Sleep(100);
    }
    r.xuliThua();
    cout<<"\t\t\t\t\t\tDiem cua ban la: "<<diem;
    return 0;
}

void gotoxy( int column, int line )
  {
  COORD coord;
  coord.X = column;
  coord.Y = line;
  SetConsoleCursorPosition(
    GetStdHandle( STD_OUTPUT_HANDLE ),
    coord
    );
  }
